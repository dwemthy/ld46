extends Area2D

# Called when the node enters the scene tree for the first time.
func _ready():
	connect("area_entered", self, "area_entered")
	
func area_entered(area):
	if area.has_method("reveal"):
		area.reveal()
