extends Area2D

export var speed = 1024
export var max_distance = 2048
export var spin_speed = 5
export var damage = 1

var travelled = 0

var velocity = Vector2()

var hit = false
var thrown = false

var weilder

signal attack_finished

func _ready():
	connect("body_entered", self, "body_entered")

func _physics_process(delta):
	if thrown:
		rotate(spin_speed * delta)
		if not hit and travelled < max_distance:
			position += velocity * delta
			travelled += velocity.length() * delta
		else:
			emit_signal("attack_finished")
				
func body_entered(body):
	hit = true
	
	if body.has_method("knock_back"):
		var to_body = body.global_position - global_position
		body.knock_back(to_body.normalized() * 600, 0.2)
	
	if body.has_method("hurt"):
		body.hurt(damage)
	elif body.has_method("die"):
		body.die()
	
func attack(direction):
	if not thrown:
		thrown = true
		travelled = 0
		velocity = direction.normalized() * speed
