extends Area2D

func _ready():
	connect("body_entered", self, "body_entered")
	$AnimationPlayer.connect("animation_finished", self, "animation_finished")

func body_entered(body):
	if body.has_method("heal"):
		body.heal(1)
		$AnimationPlayer.current_animation = "Pickup"
		
func animation_finished(which):
	if which == "Pickup":
		queue_free()
