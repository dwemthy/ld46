extends Control

func _ready():
	for file in Session.recovered_files:
		if not $Book1.visible:
			$Book1.texture = file.cover
			$Book1.show()
		elif not $Book2.visible:
			$Book2.texture = file.cover
			$Book2.show()
		elif not $Book3.visible:
			$Book3.texture = file.cover
			$Book3.show()
	Session.connect("file_recovered", self, "file_recovered")

func file_recovered(file):
		if not $Book1.visible:
			$Book1.texture = file.cover
			$Book1.show()
		elif not $Book2.visible:
			$Book2.texture = file.cover
			$Book2.show()
		elif not $Book3.visible:
			$Book3.texture = file.cover
			$Book3.show()
