extends Node2D

export(PackedScene) var Room
export(PackedScene) var Floor

export var large_room_min = 10
export var large_room_max = 20

export var medium_room_min = 8
export var medium_room_max = 15

export var small_room_min = 4
export var small_room_max = 10

export var tile_size = 64

export var large_count = 2
export var medium_count = 3

const LARGE = 0
const MEDIUM = 1
const SMALL = 2

const ENTRANCE = 0
const EXIT = 1
const TREASURE = 2
const FILE = 3
const FIGHT = 5

var room_budget = {}

class RoomConfig:
	var size: Vector2
	var pos: Vector2
	var doors: Array
	var assignment: int
	var size_type: int
	
class OpenDoor:
	var pos: Vector2
	var room: RoomConfig
	var skips = []

var open_doors = []
	
var room_configs = []

var room_assignments = {}

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	budgetRooms()
	planRooms()
	assignRooms()
	placeRooms()
	
func budgetRooms():
	room_budget = {LARGE: large_count, MEDIUM: medium_count}
	room_assignments = {SMALL: [FILE, EXIT, ENTRANCE, TREASURE]}
	room_assignments[SMALL].shuffle()
	
func vector2InRangeAsInts(range_min, range_max):
	return Vector2(intInRange(range_min, range_max), intInRange(range_min, range_max))
	
func intInRange(range_min, range_max):
	return floor(rand_range(range_min, range_max))
	
func planRooms():
	# start with one large room
	plan_room(null, large_room_min, large_room_max, 4, false, LARGE)
	
	while open_doors.size() > 0:
		var open_door = open_doors.pop_front()
		if room_budget.has(LARGE):
			room_budget[LARGE] = room_budget[LARGE] - 1
			if room_budget[LARGE] <= 0:
				room_budget.erase(LARGE)
			plan_room(open_door, large_room_min, large_room_max, 4, false, LARGE)
			
		elif room_budget.has(MEDIUM):
			room_budget[MEDIUM] = room_budget[MEDIUM] - 1
			if room_budget[MEDIUM] <= 0:
				room_budget.erase(MEDIUM)
			plan_room(open_door, medium_room_min, medium_room_max, 2 + randi() % 2, true, MEDIUM)
			
		else:
			plan_room(open_door, small_room_min, small_room_max, 0, true, SMALL)
			
func plan_room(entry_door:OpenDoor, size_min, size_max, door_count, push_front, size_const):
	var room = RoomConfig.new()
	room.doors = []
	room.size = vector2InRangeAsInts(size_min, size_max)
	room.size_type = size_const
	
	if room_assignments.has(size_const) and room_assignments[size_const].size() > 0:
		room.assignment = room_assignments[size_const].pop_front()
	else:
		room.assignment = FIGHT
	
	var doors_to_add = door_count
	var pos = Vector2()
	var skips = []
	if entry_door:
		skips = entry_door.skips.duplicate(true)
		var inner_door_pos
		var skip = Vector2()
		if entry_door.pos.x < 0:
		# This room is left of the entry
			pos.x = entry_door.room.pos.x - room.size.x - 2
			inner_door_pos = Vector2(room.size.x, intInRange(1, room.size.y - 1))
			pos.y = entry_door.room.pos.y + (entry_door.pos.y - inner_door_pos.y)
			skip.x = 1
		elif entry_door.pos.y < 0:
		# This room is above the entry
			pos.y = entry_door.room.pos.y - room.size.y - 2
			inner_door_pos = Vector2(intInRange(1, room.size.x - 1), room.size.y)
			pos.x = entry_door.room.pos.x + (entry_door.pos.x - inner_door_pos.x)
			skip.y = 1
		elif entry_door.pos.x == entry_door.room.size.x:
		# This room is right of the entry
			pos.x = entry_door.room.pos.x + entry_door.room.size.x + 2
			inner_door_pos = Vector2(-1, intInRange(1, room.size.y - 1))
			pos.y = entry_door.room.pos.y  + (entry_door.pos.y - inner_door_pos.y)
			skip.x = -1
		elif entry_door.pos.y == entry_door.room.size.y:
		#This room is below the entry
			pos.y = entry_door.room.pos.y + entry_door.room.size.y + 2
			inner_door_pos = Vector2(intInRange(1, room.size.y - 1), -1)
			pos.x = entry_door.room.pos.x + (entry_door.pos.x - inner_door_pos.x)
			skip.y = -1
			
		room.doors.append(inner_door_pos)
		
		doors_to_add -= 1
		if not skips.has(skip):
			skips.append(skip)
		
	room.pos = pos
	
	var room_doors = generateDoorPositions(room.size.x, room.size.y, doors_to_add, skips)
	
	for door_pos in room_doors:
		var door = OpenDoor.new()
		door.pos = door_pos
		door.room = room
		door.skips = skips
		room.doors.append(door_pos)
		
		if push_front:
			open_doors.push_front(door)
		else:
			open_doors.push_back(door)
	
	room_configs.append(room)
	
func assignRooms():
	pass
	
func assignRoom(room):
	pass
	
func placeRooms():
	if Room:
		spawn_goal = room_configs.size()
		for config in room_configs:
			var room = Room.instance()
			room.position = config.pos * tile_size
			room.tile_size = tile_size
			room.width = floor(config.size.x) as int
			room.height = floor(config.size.y) as int
			room.doors = config.doors
			room.assignment = config.assignment
			room.size_type = config.size_type
			room.FloorTile = Floor
			room.connect("spawned", self, "room_spawned")
			add_child(room)

var spawn_goal
var spawned = 0
func room_spawned():
	spawned += 1
	if spawned == spawn_goal:
		get_node("/root/CyberSpace").start()

func generateDoorPositions(room_width, room_height, door_count, skips):
	var low = 0.3
	var high = 0.7
	var doors = [Vector2(-1, intInRange(1 + room_height * low, (room_height - 1) * high)), 
					Vector2(intInRange(1 + room_height * low, (room_height - 1) * high), -1), 
					Vector2(intInRange(1 + room_height * low, (room_height - 1) * high), room_height), 
					Vector2(room_width, intInRange(1 + room_height * low, (room_height - 1) * high))]
	doors.shuffle()
	var generated = []
	while door_count > 0 and doors.size() > 0:
		var door = doors.pop_front()
		var valid = true
		
		for skip in skips:
			if valid and pointOnSameEdge(door, skip, room_width, room_height):
				valid = false
					
		if valid:
			door_count -= 1
			generated.append(door)
		
	return generated
	
func pointOnSameEdge(point, direction, width, height):
	return (point.x == width and direction.x > 0) or (point.x < 0 and direction.x < 0) or (point.y == height and direction.y > 0) or (point.y < 0 and direction.y < 0)
