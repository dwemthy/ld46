extends Area2D

func _ready():
	connect("body_entered", self, "body_entered")
	
func body_entered(body):
	if body.has_method("exit"):
		body.exit()
	else:
		get_node("/root/CyberSpace").exit()
