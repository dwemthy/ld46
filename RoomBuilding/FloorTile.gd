extends Area2D

export(PackedScene) var SpawnOnReveal

var bodies = 0
var active = false
var revealed = false
var locked = true

signal gremlin_died

# Called when the node enters the scene tree for the first time.
func _ready():
	connect("body_entered", self, "body_entered")
	connect("body_exited", self, "body_exited")
	
func unlock():
	if locked:
		locked = false
		if revealed:
			spawn_and_reveal()
	
func reveal():
	if not revealed:
		revealed = true
		if not locked:
			spawn_and_reveal()
				
func spawn_and_reveal():
	$AnimationPlayer.current_animation = "Reveal"
	if SpawnOnReveal:
		var spawn = SpawnOnReveal.instance()
		if "Gremlin" in spawn.name:
			spawn.connect("die", self, "gremlin_died")
		spawn.position = global_position
		get_node("/root/CyberSpace").call_deferred("add_child", spawn)
		
func gremlin_died(pos):
	emit_signal("gremlin_died", pos)

func body_entered(body):
	if not active:
		active = true
		$AnimationPlayer.current_animation = "Active"
	bodies += 1
	if body.has_method("enter_tile"):
		body.enter_tile(self)
		
func body_exited(body):
	bodies -= 1
	if active and bodies <= 0:
		active = false
		$AnimationPlayer.current_animation = "inactive"
	if body.has_method("exit_tile"):
		body.exit_tile(self)
