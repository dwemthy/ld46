extends Area2D

export(Texture) var cover
export var title = "Sloppy Joe"

func _ready():
	connect("body_entered", self, "body_entered")
	$AnimationPlayer.connect("animation_finished", self, "animation_finished")
	
func body_entered(body):
	Session.recover_file(title, cover)
	$AnimationPlayer.current_animation = "die"
	
func animation_finished(which):
	if which == "die":
		queue_free()
