extends Node2D

const LARGE = 0
const MEDIUM = 1
const SMALL = 2

const ENTRANCE = 0
const EXIT = 1
const TREASURE = 2
const FILE = 3
const FIGHT = 5

export var small_light_spawn = 1
export var small_medium_spawn = 2
export var small_heavy_spawn = 4

export var medium_light_spawn = 2
export var medium_medium_spawn = 4
export var medium_heavy_spawn = 8

export var large_light_spawn = 8
export var large_medium_spawn = 12
export var large_heavy_spawn = 16

export(PackedScene) var DaggerGremlin
export(PackedScene) var HatchetGremlin
export(PackedScene) var BombGremlin
export(PackedScene) var FloorTile
export(PackedScene) var Door
export(PackedScene) var Entrance
export(PackedScene) var Exit
export(PackedScene) var LostFile
export(PackedScene) var Health
export(PackedScene) var Treasure

export var width: int = 16
export var height: int = 16
export var tile_size = 64

export(Array, Vector2) var doors = []

var tiles = {} # { Vector2 : Node2D }

var size_type = SMALL
var assignment = FIGHT

var gremlin_count = 0

signal spawned

func _ready():
	generate_floor()
	generate_doors()
	populate()
	emit_signal("spawned")
	
func increment_gremlins():
	gremlin_count += 1
	
func decrement_gremlins(pos):
	gremlin_count -= 1
	if gremlin_count == 0:
		if Health:
			var health = Health.instance()
			health.position = pos
			get_node("/root/CyberSpace").add_child(health)

func generate_floor():
	if FloorTile:
		for x in range(0, width):
			for y in range(0, height):
				var tile = FloorTile.instance()
				var pos = Vector2(x, y)
				tile.position = pos * tile_size
				tile.connect("gremlin_died", self, "decrement_gremlins")
				add_child(tile)
				tiles[pos] = tile
				
func generate_doors():
	if FloorTile:
		for pos in doors:
			var tile = FloorTile.instance()
			tile.position = pos * tile_size
			add_child(tile)
			tiles[pos] = tile
			if Door:
				var door = Door.instance()
				door.position = pos * tile_size
				door.connect("body_entered", self, "door_entered")
				add_child(door)
				
func door_entered(body):
	unlock()

var locked = true
func unlock():
	if locked:
		locked = false
		for index in tiles:
			var tile = tiles[index]
			tile.unlock()
		
func populate():
	match assignment:
		ENTRANCE:
			var center = tiles[Vector2(floor(width * 0.5), floor(height * 0.5))].global_position
			get_node("/root/CyberSpace").set_spawn(center)
			if Entrance:
				tiles[Vector2(floor(width * 0.5), floor(height * 0.5))].SpawnOnReveal = Entrance
			unlock()
		EXIT:
			if Exit:
				tiles[Vector2(floor(width * 0.5), floor(height * 0.5))].SpawnOnReveal = Exit
			assign_medium_enemies()
		FILE:
			if LostFile:
				tiles[Vector2(floor(width * 0.5), floor(height * 0.5))].SpawnOnReveal = LostFile
			assign_medium_enemies()
		TREASURE:
			if Treasure:
				tiles[Vector2(floor(width * 0.5), floor(height * 0.5))].SpawnOnReveal = Treasure
		FIGHT:
			var weight = randi() % 10
			if weight > 8:
				assign_heavy_enemies()
			elif weight >= 4:
				assign_medium_enemies()
			else:
				assign_light_enemies()
			
func assign_light_enemies():
	var to_spawn
	if size_type == SMALL:
		to_spawn = small_light_spawn
	elif size_type == MEDIUM:
		to_spawn = medium_light_spawn
	else:
		to_spawn = large_light_spawn
	spawn_enemies(to_spawn)
	
	
func assign_medium_enemies():
	var to_spawn
	if size_type == SMALL:
		to_spawn = small_medium_spawn
	elif size_type == MEDIUM:
		to_spawn = medium_medium_spawn
	else:
		to_spawn = large_medium_spawn
	spawn_enemies(to_spawn)
	
func assign_heavy_enemies():
	var to_spawn
	if size_type == SMALL:
		to_spawn = small_heavy_spawn
	elif size_type == MEDIUM:
		to_spawn = medium_heavy_spawn
	else:
		to_spawn = large_heavy_spawn
	spawn_enemies(to_spawn)

func enemies_per_corner():
	if size_type == SMALL:
		return 1
	elif size_type == MEDIUM:
		return 2
	else:
		return 4
		
func intInRange(range_min, range_max):
	return floor(rand_range(range_min, range_max))
	
func spawn_enemies(amount):
	var visited = []
	var per_cluster = enemies_per_corner()
	
	var spawn_pos
	
	var spawned = 0
	if DaggerGremlin:
		var in_cluster = 0
		var Gremlin = DaggerGremlin
		while spawned < amount:
			Gremlin = DaggerGremlin
			var offset = Vector2(0,0)
			if in_cluster == 0:
				spawn_pos = Vector2(intInRange(0, width), intInRange(0, height))
				while visited.has(spawn_pos):
					spawn_pos = Vector2(intInRange(0, width), intInRange(0, height))
			elif in_cluster == 1:
				offset = Vector2(1, 0)
			elif in_cluster == 2:
				offset = Vector2(1, 1)
			elif in_cluster == 3:
				offset = Vector2(0, 1)
			elif in_cluster == 4:
				offset = Vector2(-1, 1)
			elif in_cluster == 5:
				offset = Vector2(-1, 0)
			elif in_cluster == 6:
				offset = Vector2(-1, -1)
			elif in_cluster == 7:
				offset = Vector2(0, -1)
			elif in_cluster == 8:
				offset = Vector2(1, -1)
				
			if spawned == 0 and BombGremlin:
				Gremlin = BombGremlin
			elif (spawned % per_cluster == 1 or spawned % per_cluster == 2) and HatchetGremlin:
				Gremlin = HatchetGremlin
			
			if tiles.has(spawn_pos + offset) and not tiles[spawn_pos + offset].SpawnOnReveal:
				# TODO mix it up!
				tiles[spawn_pos + offset].SpawnOnReveal = Gremlin
				increment_gremlins()
				spawned += 1
			in_cluster += 1
			
			if in_cluster > 8:
				in_cluster = 0
			elif spawned % per_cluster == 0:
				in_cluster = 0
