extends Sprite

export var spin_speed = 0.2

func _process(delta):
	rotate(spin_speed * delta)
