extends Node2D

export(String) var next_level

var spawn_pos
	
func set_spawn(pos):
	spawn_pos = pos
	
func start():
	if Session.RepairMan and spawn_pos:
		var repairMan = Session.RepairMan.instance()
		repairMan.position = spawn_pos
		if Session.player_health < 0:
			Session.player_health = repairMan.max_health
			
		repairMan.health = Session.player_health
			
		call_deferred("add_child", repairMan)
		$Sigmoidoscope.set_repair_man(repairMan)
		$Sigmoidoscope.position = repairMan.position

func exit():
	if next_level:
		get_tree().change_scene(next_level)
	else:
		Session.win()
