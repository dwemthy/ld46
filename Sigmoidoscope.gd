extends Camera2D

export var follow_speed = 256
export var follow_accel = 512
export var follow_threshold = 256

var repairman
var velocity = Vector2()

func _ready():
	set_repair_man(get_node("/root/CyberSpace/RepairMan"))
		
func set_repair_man(man):
	if man:
		repairman = man
		repairman.connect("die", self, "repairman_died")
	
	
func repairman_died():
	repairman = null
	
func _process(delta):
	if repairman:
		var to_repairman = repairman.position - position
		if to_repairman.length() > follow_threshold:
			velocity = to_repairman.normalized() * (velocity.length() + follow_accel * delta)
			if velocity.length() > follow_speed:
				velocity = velocity.normalized() * follow_speed
		else:
			var current_speed = velocity.length()
			velocity = to_repairman.normalized() * current_speed * 0.75
			if velocity.length() * delta >= to_repairman.length():
				velocity = Vector2()
				
		position += velocity * delta
		
