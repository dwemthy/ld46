extends Control

func _process(delta):
	$ProgressBar.value = Session.player_health
	$Amount.text = String(Session.player_health) + " / 10"
