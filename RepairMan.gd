extends KinematicBody2D

export(PackedScene) var Weapon
export(PackedScene) var PickupTemplate
export var speed = 128
export var accel = 256

var velocity = Vector2()
var input = Vector2(0,0)

var weapon
var max_health = 10
var health
var falling = false
var dying = false

signal die

func _ready():
	$AnimationPlayer.connect("animation_finished", self, "animation_finished")
	$PickupLabel.hide()
	if Session.Weapon:
		equip(Session.Weapon)

func _physics_process(delta):
	if falling:
		play_sprite_animation("WalkUp")
		scale -= Vector2(delta, delta)
		rotate(delta * 5)
		if scale.x <= 0 or scale.y <= 0:
			die()
			
	if dying:
		return
			
	var input = Vector2(0,0)
	if Input.is_action_pressed("left"):
		input.x -= 1
	if Input.is_action_pressed("right"):
		input.x += 1
	if Input.is_action_pressed("up"):
		input.y -= 1
	if Input.is_action_pressed("down"):
		input.y += 1
		
	if Input.is_action_pressed("attack"):
		if not weapon:
			weapon = Weapon.instance()
			weapon.weilder = self
			weapon.position = position
			weapon.connect("attack_finished", self, "attack_finished")
			get_parent().call_deferred("add_child", weapon)
			
		var to_mouse = get_global_mouse_position() - position
		weapon.attack(to_mouse)
		
	if available_pickup and Input.is_action_just_pressed("pickup"):
		if PickupTemplate and Weapon:
			var weapon_instance = Weapon.instance() 
			var pickup = PickupTemplate.instance()
			pickup.Weapon = Weapon
			pickup.get_node("Sprite").texture = weapon_instance.get_node("Sprite").texture
			pickup.position = position
			get_parent().call_deferred("add_child", pickup)
		equip(available_pickup.Weapon)
		$PickupLabel.hide()
		available_pickup.pickup()
	
	if input.length() > 0:
		velocity += input * accel
		if velocity.length() > speed:
			velocity = velocity.normalized() * speed
	else:
		var current_speed = velocity.length()
		if current_speed <= accel * 2 * delta:
			velocity = Vector2()
		else:
			velocity = velocity.normalized() * (current_speed - accel * 2 * delta)
	
	velocity = move_and_slide(velocity)
	update_facing()
	
func update_facing():
	if abs(velocity.x) > abs(velocity.y):
		if velocity.x > 0:
			play_sprite_animation("WalkRight")
		else:
			play_sprite_animation("WalkLeft")
	elif abs(velocity.y) > abs(velocity.x):
		if velocity.y > 0:
			play_sprite_animation("WalkDown")
		else:
			play_sprite_animation("WalkUp")
	else:
		play_sprite_animation("Idle")
		
func play_sprite_animation(which):
	if not which == $SpriteAnimator.current_animation:
		$SpriteAnimator.current_animation = which
	
func attack_finished():
	if weapon:
		weapon.queue_free()
		weapon = null
	
var tile_count = 0
func enter_tile(tile):
	tile_count += 1

func exit_tile(tile):
	tile_count -= 1
	if tile_count <= 0:
		fall()
		
func fall():
	falling = true
	dying = true
	if weapon:
		weapon.weilder = null
		weapon.queue_free()
		weapon = null
		
func knock_back(force, duration = 0.5):
	velocity = force
	
var available_pickup = null

func near_pickup(pickup):
	$PickupLabel.show()
	available_pickup = pickup
	
func lose_pickup():
	$PickupLabel.hide()
	available_pickup = null
	
func equip(to_equip):
	if weapon:
		weapon.weilder = null
		weapon.queue_free()
		weapon = null
		
	Weapon = to_equip
	Session.Weapon = to_equip
		
func hurt(amount):
	health -= amount
	Session.player_health = health
	if health <= 0:
		die()
		
func heal(amount):
	if health < max_health:
		health += amount
		if health > max_health:
			health = max_health
		Session.player_health = health

func die():
	if weapon:
		weapon.weilder = null
		weapon.queue_free()
		weapon = null
	emit_signal("die")
	Session.lose()
	$AnimationPlayer.current_animation = "Die"
	
func exit():
	$AnimationPlayer.current_animation = "Exit"

func animation_finished(which):
	if which == "Die":
		queue_free()
	elif which == "Exit":
		get_node("/root/CyberSpace").exit()
