extends KinematicBody2D

export var speed = 700
export(PackedScene) var Weapon
export(PackedScene) var Pickup
export var max_health = 1

export var attack_cooldown = 2.0

var weapon
var player
var cooled = 0
var falling = false
var health

var knockback_force
var knockback_remaining

signal die

func _ready():
	health = max_health
	$Vision.connect("body_entered", self, "body_entered")
	$AnimationPlayer.connect("animation_finished", self, "animation_finished")
	
func _physics_process(delta):
	var velocity = Vector2()
	if knockback_remaining and knockback_remaining > 0:
		knockback_remaining -= delta
		move_and_slide(knockback_force)
	elif dying:
		pass # don't do stuff
	elif falling:
		play_sprite_animation("WalkUp")
		scale -= Vector2(delta, delta)
		rotate(delta * 5)
		if scale.x <= 0 or scale.y <= 0:
			die()
	elif not weapon:
		cooled += delta
		if player:
			var closest_tile = tiles[0]
			var to_closest = closest_tile.global_position - global_position
			for tile in tiles:
				if tile != closest_tile:
					var to_tile = tile.global_position - global_position
					if to_tile.length() < to_closest.length():
						closest_tile = closest_tile
						to_closest = to_tile
			var to_player = player.position - global_position
			var tile_to_player = player.position - closest_tile.global_position
			var space = get_world_2d().direct_space_state
			var end_raycast = closest_tile.global_position + tile_to_player.normalized() * 150
			var result = space.intersect_ray(closest_tile.global_position, end_raycast, [closest_tile], 1+2+4+8+16, false, true)
			velocity = to_player
			if result:
				if cooled >= attack_cooldown:
					attack()
				elif player:
					move_and_slide(to_player.normalized() * speed)
						
	update_facing(velocity)
	
func update_facing(velocity):
	if abs(velocity.x) > abs(velocity.y):
		if velocity.x > 0:
			play_sprite_animation("WalkRight")
		else:
			play_sprite_animation("WalkLeft")
	elif abs(velocity.y) > abs(velocity.x):
		if velocity.y > 0:
			play_sprite_animation("WalkDown")
		else:
			play_sprite_animation("WalkUp")
	else:
		play_sprite_animation("Idle")
		
func play_sprite_animation(which):
	if not which == $SpriteAnimator.current_animation:
		$SpriteAnimator.current_animation = which
		
func body_entered(body):
	if body.name == "RepairMan":
		player = body
		player.connect("die", self, "lose_player")
		cooled = 0
		
func hurt(damage):
	health -= damage
	if health <= 0:
		die()
		
func lose_player():
	player = null
	
func attack():
	if player and Weapon:
		weapon = Weapon.instance()
		weapon.weilder = self
		weapon.position = global_position
		weapon.connect("attack_finished", self, "attack_finished")
		get_node("/root/CyberSpace").call_deferred("add_child", weapon)
		cooled = 0
		var to_player = player.position - global_position
		weapon.attack(to_player)

var tiles = []
func enter_tile(tile):
	tiles.append(tile)

func exit_tile(tile):
	tiles.erase(tile)
	if tiles.size() <= 0:
		fall()
		
func attack_finished():
	if weapon:
		weapon.queue_free()
		weapon = null
	
func knock_back(force, duration = 0.5):
	knockback_force = force
	knockback_remaining = duration
	
func fall():
	if weapon:
		weapon.weilder = null
		weapon.queue_free()
		weapon = null
	falling = true

var dying = false
func die():
	if weapon:
		weapon.weilder = null
		weapon.queue_free()
		weapon = null
	if Pickup:
		var pickup = Pickup.instance()
		pickup.position = global_position
		get_node("/root/CyberSpace").call_deferred("add_child", pickup)
		
	emit_signal("die", global_position)
	dying = true
	$AnimationPlayer.current_animation = "Die"

func animation_finished(which):
	if which == "Die":
		queue_free()
