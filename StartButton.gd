extends TextureButton

export(PackedScene) var RepairMan

func _ready():
	connect("button_up", self, "start")
	
func start():
	Session.RepairMan = RepairMan
	Session.start()
