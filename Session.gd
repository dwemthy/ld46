extends Node

export(PackedScene) var RepairMan
export(PackedScene) var Weapon

signal file_recovered

var recovered_files = []

var player_health = -1

class LostFile:
	var cover: Texture
	var title: String

func recover_file(title, cover):
	var file = LostFile.new()
	file.title = title
	file.cover = cover
	recovered_files.append(file)
	
	emit_signal("file_recovered", file)

func start():
	get_tree().change_scene("res://Levels/SloppyJoe.tscn")

func lose():
	pass #TODO

func win():
	get_tree().change_scene("res://End.tscn")
