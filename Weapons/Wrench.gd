extends Area2D

export var speed = 1024
export var max_distance = 2048
export var spin_speed = 5
export var end_wait = 0.1
export var damage = 1

var travelled = 0

var velocity = Vector2()

var hit = false
var thrown = false

var weilder

var waited = 0

signal attack_finished

func _ready():
	waited = end_wait
	connect("body_entered", self, "body_entered")

func _physics_process(delta):
	if weilder:
		if thrown:
			rotate(spin_speed * delta)
			if not hit and travelled < max_distance:
				position += velocity * delta
				travelled += velocity.length() * delta
			else:
				var to_weilder = weilder.global_position - position
				if to_weilder.length() <= speed * delta:
					position = weilder.global_position
					thrown = false
				else:
					position += to_weilder.normalized() * speed * delta
		else:
			position = weilder.global_position
			waited += delta
			if waited >= end_wait:
					emit_signal("attack_finished")
				
func body_entered(body):
	hit = true
	
	if body.has_method("knock_back"):
		var to_body = body.global_position - global_position
		body.knock_back(to_body.normalized() * 500, 0.2)
	
	if body.has_method("hurt"):
		body.hurt(damage)
	elif body.has_method("die"):
		body.die()
	
func attack(direction):
	if not thrown and waited >= end_wait:
		thrown = true
		travelled = 0
		waited = 0
		velocity = direction.normalized() * speed
