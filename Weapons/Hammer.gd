extends Area2D

export var swing_speed = 5.0
export var swing_range = 1.5
export var end_wait = 0.1
export var damage = 2

signal attack_finished

var swinging = false
var swing_end
var waited = 0
var swing_dir = 1

var weilder

func _ready():
	waited = end_wait
	connect("body_entered", self, "body_entered")
	
func body_entered(body):
	if body.has_method("knock_back"):
		var to_body = body.global_position - global_position
		body.knock_back(to_body.normalized() * 1000, 0.3)
		
	if body.has_method("hurt"):
		body.hurt(damage)
	elif body.has_method("die"):
		body.die()

func _physics_process(delta):
	if weilder:
		position = weilder.position
		if swinging:
			var to_rotate = delta * swing_speed
			if to_rotate <= rotation - swing_end:
				to_rotate = rotation - swing_end
				swinging = false
				
			rotate(to_rotate)
		else:
			waited += delta
			if waited >= end_wait:
				emit_signal("attack_finished")
				$CPUParticles2D.emitting = true
	
func attack(direction):
	if not swinging and waited >= end_wait:
		$CPUParticles2D.emitting = true
		var center_angle = direction.angle()
		rotation = center_angle  - (swing_range * 0.5)
		swing_end = rotation + swing_range
		swinging = true
		waited = 0
