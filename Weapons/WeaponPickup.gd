extends Area2D

export(PackedScene) var Weapon

var acquired = false

func _ready():
	connect("body_entered", self, "body_entered")
	connect("body_exited", self, "body_exited")
	$AnimationPlayer.connect("animation_finished", self, "animation_finished")
	
func body_entered(body):
	if not acquired and body.has_method("near_pickup"):
		body.near_pickup(self)
		
func body_exited(body):
	if not acquired and body.has_method("lose_pickup"):
		body.lose_pickup()

func pickup():
	if not acquired:
		acquired = true
		$AnimationPlayer.current_animation = "pickup"

func animation_finished(which):
	if which == "pickup":
		queue_free()
