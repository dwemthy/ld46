extends Area2D

export var speed = 1024
export var decel = 512
export var damage = 3
export var fuse = 1.5
export var knock_back = 1200

var velocity = Vector2()

var hit = false
var thrown = false

var weilder

signal attack_finished

func _ready():
	connect("body_entered", self, "body_entered")

func _physics_process(delta):
	if thrown:
		fuse -= delta
		if fuse <= 0:
			for body in $Explosion.get_overlapping_bodies():
				if body.has_method("knock_back"):
					var to_body = body.global_position - global_position
					body.knock_back(to_body.normalized() * knock_back, 0.2)
				
				if body.has_method("hurt"):
					body.hurt(damage)
				elif body.has_method("die"):
					body.die()
					
			emit_signal("attack_finished")
		elif velocity.length() > 0:
			position += velocity * delta
			if velocity.length() < decel * delta:
				velocity = Vector2()
			else:
				velocity -= velocity.normalized() * delta * decel
				
func body_entered(body):
	if not body == weilder:
		hit = true
		velocity = Vector2()
	
func attack(direction):
	if not thrown:
		$AnimationPlayer.current_animation = "TickTick"
		thrown = true
		velocity = direction.normalized() * speed
