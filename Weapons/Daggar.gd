extends Area2D

export var speed = 1024
export var thrust_distance = 300
export var damage = 1
export var knock_back = 200
export var end_wait = 0.1

var travelled = 0
var waited = 0

var velocity = Vector2()

var hit = false

var weilder

var thrusting = false

signal attack_finished

func _ready():
	waited = end_wait
	connect("body_entered", self, "body_entered")

func _physics_process(delta):
	if weilder:
		var offset = Vector2()
		if thrusting:
			if not hit and travelled < thrust_distance:
				if weilder.has_method("move_and_slide"):
					weilder.move_and_slide(velocity)
				travelled += velocity.length() * delta
				var offset_percent = (travelled / thrust_distance) * 2
				if offset_percent > 1.0:
					offset_percent = 2.0 - offset_percent
				offset = 75 * offset_percent * velocity.normalized()
			else:
				thrusting = false
		else:
			waited += delta
			if waited >= end_wait:
				emit_signal("attack_finished")
				
		position = weilder.global_position + offset
	else:
		emit_signal("attack_finished")
		
				
func body_entered(body):
	hit = true
	
	if body.has_method("knock_back"):
		var to_body = body.global_position - global_position
		body.knock_back(to_body.normalized() * knock_back)
		
	if body.has_method("hurt"):
		body.hurt(damage)
	elif body.has_method("die"):
		body.die()
		
	
func attack(direction):
	if not thrusting:
		thrusting = true
		travelled = 0
		waited = 0
		rotation = direction.angle()
		velocity = direction.normalized() * speed
